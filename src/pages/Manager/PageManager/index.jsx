import styles from './CallReport.module.css';
import DatePicker from 'react-datepicker';
import vi from 'date-fns/locale/vi';
import 'react-datepicker/dist/react-datepicker.css';
import { useState } from 'react';
import ChartCircleCall from '../../../components/Charts/ChartCircleCall';
import ChartCallMonth from '../../../components/Charts/ChartCallMonth';
import { useQuery } from '@tanstack/react-query';
import { useAxios } from '../../../context/AxiosContex';
import { useAuth } from '../../../context/AuthContext';
import { toast } from 'react-hot-toast';
import {  useEffect } from 'react'; 
export default function ManagerManager() {
  const [selectMonth, setSelectMonth] = useState(new Date());
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 1 * 24 * 60 * 60 * 1000),
  );
  const [endDate, setEndDate] = useState(
    new Date(new Date().getTime() + 1* 60 * 60 * 1000),
  );
  
  const { getReportCall,getTicket } = useAxios();
  const [callData, setCallData] = useState([]);
  const { isLoading, error, data: ticket } = useQuery({
    queryKey: ['ticket', startDate, endDate], // Corrected queryKey
    queryFn: async () => {
      return await getReportCall({
        startDate: startDate.toISOString(),
        endDate: endDate.toISOString(),
      });
    },
  });
  // useEffect(() => {
  //   if (htrCall) {
  //     setCallData(htrCall);
  //     console.log(htrCall);
  //   }
  //   else{
  //     console.log('sao có giá trị mà ko in');
  //   }
  // }, [htrCall]);
  return (
    <>
     <div className={styles.callsboard}>
      <div className={styles.reportCall}>
        <h5 className={styles.title}>Báo cáo ngày</h5>
        <div className={styles.dates}>
          <div>
            <DatePicker
              locale={vi}
              selected={startDate}
              onChange={(date) => {
                if (endDate - date > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setStartDate(date);
              }}
            />
          </div>
          <strong> Tới</strong>
          <div>
            <DatePicker
              locale={vi}
              selected={endDate}
              onChange={(date) => {
                if (date - startDate > 31 * 24 * 60 * 60 * 1000) {
                  toast('Chỉ xử lý được trong khoảng 30 ngày', {
                    icon: '👏',
                    style: {
                      borderRadius: '10px',
                      background: '#333',
                      color: '#fff',
                    },
                  });
                  return;
                }
                return setEndDate(date);
              }}
            />
          </div>
        </div>
        <div className={styles.statistical}>
        <h4>Thống kê theo ngày: {startDate.toISOString().split('T')[0]} Tới ngày {endDate.toISOString().split('T')[0]}</h4>
          <div className={styles.charts}>
            <div className={styles.chart}>
              <h5>Thống kê cuộc gọi</h5>
              <div>
                <ChartCircleCall htrCall={ticket} />
              </div>
            </div>
            <div className={styles.listCall}>
              <div className={styles.box__content}>
                <div className={styles.head__box}>
                  <h3>Cuộc gọi chi tiết theo ngày</h3>
                </div>
                <div className={styles.head_new}>
                  <span>STT</span>
                  <span>Ngày</span>
                  <span>Nghe máy</span>
                  <span>Tắt máy</span>
                  <span>Máy bận</span>
              
                  <span>Tổng</span>
                </div>
                
                {ticket?.listCall &&
                  ticket?.listCall
                    .sort(function (a, b) {
                      return (
                        new Date(b.date).getTime() - new Date(a.date).getTime()
                      );
                    })
                    .map((htr, index) => (
                      <div
                        key={index}
                        className={styles.content_new}
                      >
                        <span>{index + 1}</span>
                        <span>{htr.date}</span>
                        <span className={`${htr.answer != 0 && styles.bold}`}>
                          {htr.answer}
                        </span>
                     
                        <span className={`${htr.reject != 0 && styles.bold3}`}>
                          {htr.reject}
                        </span>
                        <span
                          className={`${htr.noAnswer != 0 && styles.bold4}`}
                        >
                          {htr.noAnswer}
                        </span>
                        <span>
                          {htr.noAnswer +
                            htr.answer +
                            htr.reject}
                        </span>
                      </div>
                    ))}

              </div>
            </div>
          </div>
        </div>
        <div className={styles.reportDate}>
        <h5 className={styles.title}>Báo cáo tháng</h5>
        <div className={styles.dates}>
          <div>
            <DatePicker
              selected={selectMonth}
              onChange={(date) => setSelectMonth(date)}
              dateFormat='MM/yyyy'
              locale={vi}
              showMonthYearPicker
            />
          </div>
        </div>

        {/* <div className={styles.statistical}>
          <h4>Thống kê theo tháng</h4>
        </div> */}

      </div>
      </div>
    </div>
    </>
  );
}