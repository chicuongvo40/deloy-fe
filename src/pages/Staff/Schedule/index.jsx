import React from 'react';
import vi from 'date-fns/locale/vi';
import styles from './CallReport.module.css';
import {
  Box,
  List,
  ListItem,
  ListItemText,
  Typography,
  Button,
  Modal,
  TextField
} from "@mui/material";
import DatePicker from 'react-datepicker';
import { format } from 'date-fns';
import FullCalendar from "@fullcalendar/react";
import { formatDate } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import listPlugin from '@fullcalendar/list';
import interactionPlugin from '@fullcalendar/interaction';
import { useQuery } from '@tanstack/react-query'; // Removed useMutation as it's not used
import { DataGrid } from "@mui/x-data-grid";
import { useAxios } from '../../../context/AxiosContex';
import { useState, useEffect } from 'react'; // Removed useRef and useCallback as they're not used
import { formartDate, formatNumber } from '../../../utils/functions';
import { useMutation } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';
import { useRef } from 'react';
import { set } from 'date-fns';

export default function Schedule() {

  //Api link
  const { getschedule, postschedule, getScheduleByTitle } = useAxios();
  //Usestate
  const [startDate, setStartDate] = useState(
    new Date(new Date().getTime() - 11 * 24 * 60 * 60 * 1000),
  );
  const [customerData, setCustomerData] = useState([]);
  const [open, setOpen] = useState(false);
  const [editopen, setEditOpen] = useState(false);
  const [info, setInfo] = useState("");
  const [title, setTitle] = useState("");
  const [scheduleData, setScheduleData] = useState("");
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const handleEditClose = () => setEditOpen(false);
  const [currentEvents, setCurrentEvents] = useState([]);
  const [currentEvents1, setCurrentEvents1] = useState([]);
  const customerId = useRef();
  const staffId = useRef();
  const tittle = useRef();
  const note = useRef();

  const [customerId1, setcustomerId] = useState('');
  const [staffId1, setstaffId] = useState('');
  const [tittle1, settittle] = useState('');
  const [note1, setnote] = useState('');
  const postCustomerMutation = useMutation({
    mutationFn: (data) => {
      return postschedule(data).then((res) => {
        return res;
      });
    },
    onSuccess(data) {
      console.log(data);
      if (data?.status !== 400) {
        refetch();
        toast('Tạo thành công', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      } else {
        toast('Có lỗi trong quá trình tạo schedule', {
          icon: '👏',
          style: {
            borderRadius: '10px',
            background: '#333',
            color: '#fff',
          },
        });
      }
    },
  });


  //* Hàm tạo khách mới
  const handleAddBrand = (e) => {
    e.preventDefault();
    const calendarApi = info.view.calendar;
    calendarApi.addEvent({
      id: `id` + info.startStr,
      title: tittle.current.value,
      start: info.startStr,
      end: info.endStr,
      allDay: info.allDay,
    });
    if (
      !customerId.current.value ||
      !staffId.current.value ||
      !tittle.current.value ||
      !note.current.value
    ) {
      toast('Không được bỏ trống', {
        icon: '👏',
        style: {
          borderRadius: '10px',
          background: '#333',
          color: '#fff',
        },
      });
      return;
    }
    const data = {
      customerId: customerId.current.value,
      staffId: staffId.current.value,
      tittle: tittle.current.value,
      status: 'Mới',
      note: note.current.value,
      createdDate: format(Date.now(), 'yyyy-MM-dd'),
      meetTime: info.startStr,
      lastEditedTime: format(Date.now(), 'yyyy-MM-dd')
    };
    console.log(data);
    postCustomerMutation.mutate({
      data
    });
  };
  //GetSchedule
  const {
    data: customers,
    isLoading,
    refetch,
  } =
    useQuery(
      {
        queryKey: ['customers'],
        queryFn: async () => await getschedule(),
      }
    );

  const {
    data: schedule,
  } =
    useQuery(
      {
        queryKey: ['schedule', title],
        queryFn: async () => await getScheduleByTitle(title),
      }
    );

  useEffect(() => {
    if (schedule) {
      setstaffId(schedule.staffId);
      setcustomerId(schedule.customerId);
      settittle(schedule.tittle);
      setnote(schedule.note)
    }
  }, [schedule]);

  useEffect(() => {
    if (customers) {
      const events = [];
      customers.forEach(customer => {
        events.push({
          id: customer.id,
          title: customer.tittle,
          date: customer.meetTime,
        });
      });
      setCurrentEvents1(events);
      console.log(customers);
    }
  }, [customers]);

  const handleDateClick = (selected) => {
    setOpen(true);
    setInfo(selected);

    const calendarApi = selected.view.calendar;
    calendarApi.unselect();
  };

  const handleEventClick = (selected) => {
    setInfo(selected);
    if (selected) {
      setTitle(selected.event._def.title)
      setEditOpen(true);
    } else {
      console.log('Không c');
    }
  };

  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    boxShadow: 'none', // hoặc boxShadow: 0,
    p: 4,
  };


  return (
    <>
      {open && (
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box>
              <Typography id="modal-modal-title" variant="h6" component="h2">
                Bạn muốn tạo 1 lịch hẹn lúc {info.startStr}
              </Typography>
              <TextField
                type="text"
                name="Customer"
                id="standard-basic"
                label="Customer"
                sx={{ mb: 2, width: '100%' }}
                inputRef={customerId}

              />
              <TextField
                type="text"
                name="Staff"
                id="standard-basic"
                label="Staff"
                sx={{ mb: 2, width: '100%' }}
                inputRef={staffId}
              />
              <TextField
                type="text"
                name="Title"
                id="standard-basic"
                label="Title"
                sx={{ mb: 2, width: '100%' }}
                inputRef={tittle}
              />
              <TextField
                type="text"
                name="Note"
                id="standard-basic"
                label="Note"
                sx={{ mb: 2, width: '100%' }}
                inputRef={note}
              />
            </Box>
            <Box>
              <Button onClick={handleClose} sx={{ bgcolor: 'red', color: 'white', mr: 1 }}>Đóng</Button> {/* Nút button Đóng với sự kiện onClose */}
              <Button onClick={handleAddBrand}
              sx={{ bgcolor: 'blue', color: 'white' }}
              >
                Tạo
              </Button>
            </Box>
          </Box>
        </Modal>
      )}

      {editopen && (
        <Modal
          open={editopen}
          onClose={handleEditClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>
            <Box>
              <Typography
                id="modal-modal-title"
                variant="h6"
                component="h2"
                sx={{
                  fontWeight: 'bold',
                  fontSize: '20px', // Điều chỉnh kích thước phù hợp
                  color: 'blue', // Màu chữ xanh dương
                  textShadow: '2px 2px 4px rgba(0, 0, 0, 0.2)', // Hiệu ứng bóng đổ
               
                  marginBottom: '20px' // Khoảng cách dưới
                }}
              >
                Bạn muốn Edit
              </Typography>
              <TextField
                type="text"
                name="Customer"
                id="standard-basic"
                sx={{ mb: 2, width: '100%' }}
                value={customerId1}
              />
              <TextField
                type="text"
                name="Staff"
                id="standard-basic"
                sx={{ mb: 2, width: '100%' }}
                value={staffId1}
              />
              <TextField
                type="text"
                name="Title"
                id="standard-basic"
                sx={{ mb: 2, width: '100%' }}
                value={tittle1}
              />
              <TextField
                type="text"
                name="Note"
                id="standard-basic"
                sx={{ mb: 2, width: '100%' }}
                value={note1}
              />

            </Box>
            <Box>
              <Button onClick={handleClose} sx={{ bgcolor: 'red', color: 'white', mr: 1 }}>
                Đóng
              </Button>
              <Button  sx={{ bgcolor: 'blue', color: 'white' }}>
                Tạo
              </Button>
            </Box>
          </Box>

        </Modal>
      )}

      {
        customerData ? (<Box style={{ width: '100%', height: '100vh' }}
        >
          <Box m="20px">
            <Box display="flex" justifyContent="space-between">
              {/* CALENDAR SIDEBAR */}
              <Box
                flex="1 1 20%"
                backgroundColor='white'
                p="15px"
                borderRadius="4px"
              >
                <Typography variant="h5">Events</Typography>
                <List>
                  {currentEvents.map((event) => (
                    <ListItem
                      key={event.id}
                      sx={{
                        backgroundColor: "rgb(190, 190, 190,100)",
                        margin: "10px 0",
                        borderRadius: "2px",
                      }}
                    >
                      <ListItemText
                        primary={event.title}
                        secondary={
                          <Box>
                            <Typography>
                              {formatDate(event.start, {
                                year: "numeric",
                                month: "short",
                                day: "numeric",
                              })}
                            </Typography>
                            <Typography>
                              {event.id}
                            </Typography>
                          </Box>
                        }
                      />
                    </ListItem>
                  ))}
                </List>
              </Box>
              <Box flex="1 1 100%" ml="15px">
                <FullCalendar
                  height="75vh"
                  plugins={[
                    dayGridPlugin,
                    timeGridPlugin,
                    interactionPlugin,
                    listPlugin,
                  ]}
                  headerToolbar={{
                    left: "prev,next today",
                    center: "title",
                    right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth",
                  }}
                  initialView="dayGridMonth"
                  editable={true}
                  selectable={true}
                  selectMirror={true}
                  dayMaxEvents={true}
                  select={handleDateClick}
                  eventClick={handleEventClick}
                  eventsSet={(events) => setCurrentEvents(events)}
                  //   initialEvents={customerData.map(customer => ({
                  //     id: customer.id,
                  //     title: customer.title, // Corrected typo from customer.tittle to customer.title
                  //     date: customer.meetTime,
                  // }))}
                  events={currentEvents1}
                />
              </Box>
            </Box>
          </Box>
        </Box>) : null
      }
    </>
  );
}
