import React from 'react';
import {
  Chip ,
  Box,
  Card,
  CardActions,
  CardContent,
  Button,
  Typography,
  Grid
} from "@mui/material";
import { useNavigate } from 'react-router-dom';
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
//Context
import { useAxios } from '../../../context/AxiosContex';
//Utils
import { formartDate, formatNumber } from '../../../utils/functions';
//Components
import StatCards from '../../../components/StatCards';

const Product = ({
  tag,
  color,
  levelId,
  id,
  customerId,
  ticketTypeId,
  note,
  ticketStatusId,
  createdBy
}) => {
  const noteStyle = {
    maxHeight: 'calc(1.5em * 4)', // Giới hạn chiều cao tối đa là 4 hàng
    overflow: 'hidden',
    display: '-webkit-box',
    WebkitLineClamp: 4,
    WebkitBoxOrient: 'vertical',
  };
  return (
    <Card
      sx={{
        backgroundImage: "none",
        backgroundColor: 'white',
        borderRadius: "0.55rem",
      }}
    >
      <CardContent sx={{ margin: '0' }}>
        <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Typography sx={{ fontSize: 14, fontWeight: 'bold' }} color='black'>
            Level: {levelId}
          </Typography>
          <Box sx={{ backgroundColor: color, height: '3vh', width: '3vw' }}>
          </Box>
        </Box>

        <div style={{ display: 'grid', gridTemplateColumns: 'repeat(2, 1fr)', gap: '8px' }}>
          {tag ? (
            // Sử dụng map để lặp qua mảng tag và render mỗi phần tử trong một phần tử Typography
            tag.map((tagItem, index) => (
              <Chip label={tagItem} size="small" key={index} color="primary" variant="outlined" sx={{ fontSize: 10, fontWeight: 'bold' }} >
                #{tagItem}
              </Chip  >
            ))
          ) : (
            // Nếu mảng tag rỗng, không hiển thị gì cả
            null
          )}
        </div>


        <Typography color='blue' sx={{ fontSize: 14, fontWeight: 'bold' }}>
          Phone: {customerId}
        </Typography>

        <Typography variant="body2" style={noteStyle}>Nội Dung :{note}</Typography>

        <Box sx={{ display: 'flex', justifyContent: 'space-between', margin: '0' }}>
          <div>
            #TK12934{id}
          </div>
          <div>
            {formartDate(createdBy, 'short')}
          </div>
        </Box>
      </CardContent>
    </Card>

  );
};


export default function Ticket() {
  //Axios
  const { getTicketByStatus } = useAxios();
  //Usestate
  const [ticketData, setTicketData] = useState([]);
  const [ticketData1, setTicketData1] = useState([]);
  const [ticketData2, setTicketData2] = useState([]);
  const [ticketData3, setTicketData3] = useState([]);
  //Navigation
  const navigate = useNavigate();
  //GetTicket
  const {
    data: ticket,
  } = useQuery(
    {
      queryKey: ['ticket'],
      queryFn: () => getTicketByStatus(1),
    }
  );
  const {
    data: ticket1,
  } = useQuery(
    {
      queryKey: ['ticket1'],
      queryFn: () => getTicketByStatus(2),
    }
  );
  const {
    data: ticket2,
  } = useQuery(
    {
      queryKey: ['ticket2'],
      queryFn: () => getTicketByStatus(3),
    }
  );
  const {
    data: ticket3,
  } = useQuery(
    {
      queryKey: ['ticket3'],
      queryFn: () => getTicketByStatus(4),
    }
  );

  useEffect(() => {
    if (ticket) {
      setTicketData(ticket);
      console.log(ticket);
    }
  }, [ticket]);
  useEffect(() => {
    if (ticket1) {
      setTicketData1(ticket1);
      console.log(ticket1);
    }
  }, [ticket1]);
  useEffect(() => {
    if (ticket2) {
      setTicketData2(ticket2);
      console.log(ticketData2.length);
    }
  }, [ticket2]);
  useEffect(() => {
    if (ticket3) {
      setTicketData3(ticket3);
      console.log(ticketData3.length);
    }
  }, [ticket3]);

  return (
    <>

      <Box m="1.5rem" gridTemplateColumns="repeat(4,1fr)" width='80vw'>
        <StatCards />
        <Grid container spacing={2}>
          <Grid item xs={3}>
            {ticket ? (
              <Box mt="20px">
                {ticket.map(
                  ({
                    tags,
                    levelId,
                    id,
                    callId,
                    ticketTypeId,
                    note,
                    ticketStatusId,
                    createdBy
                  }, index) => (
                    <Box key={id} mb="10px">
                      <Product
                        tag={tags}
                        color='lightgreen'
                        levelId={levelId}
                        id={id}
                        customerId={callId}
                        ticketTypeId={ticketTypeId}
                        note={note}
                        ticketStatusId={ticketStatusId}
                        createdBy={createdBy}
                      />
                    </Box>
                  )
                )}
              </Box>
            ) : (
              <>Loading...</>
            )}
          </Grid>

          <Grid item xs={3}>
            {ticket1 ? (
              <Box mt="20px">
                {ticket1.map(
                  ({
                    tags,
                    levelId,
                    id,
                    customerId,
                    ticketTypeId,
                    note,
                    ticketStatusId,
                    createdBy,
                    callId
                  }, index) => (
                    <Box key={id} mb="10px">
                      <Product
                        tag={tags}
                        color='lightpink'
                        levelId={levelId}
                        id={id}
                        customerId={callId}
                        ticketTypeId={ticketTypeId}
                        note={note}
                        ticketStatusId={ticketStatusId}
                        createdBy={createdBy}
                      />
                    </Box>
                  )
                )}
              </Box>
            ) : (
              <>Loading...</>
            )}
          </Grid>

          <Grid item xs={3}>

          </Grid>





        </Grid>

      </Box>

    </>
  );
}
