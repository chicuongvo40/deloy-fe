import React from 'react'
import { useQuery } from '@tanstack/react-query';
import { useState, useEffect } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { AiFillFileExcel } from 'react-icons/ai';
import {
  Box,
  Icon,
  Paper, Grid, Divider,
  IconButton,
  Typography,
} from "@mui/material";
import { useParams } from 'react-router-dom';
import { Edit as EditIcon, PriorityHigh as PriorityHighIcon } from '@mui/icons-material';
import { useRef, useCallback } from 'react';
import { formartDate, formatNumber } from '../../../utils/functions';
import { FcCallback } from 'react-icons/fc';
import { CgDetailsMore } from 'react-icons/cg';
import DataGridCustomToolbar from "../../../components/DataGridCustomToolbar/DataGridCustomToolbar";
import { DataGrid } from "@mui/x-data-grid";
import { useAxios } from '../../../context/AxiosContex';
import { TiDelete } from 'react-icons/ti';
import AddCampaignAdm from '../../../components/AdminCpns/AddStaff';
import './TicketDetail.css';
export default function StaffManager() {

  const { getTicketById,getCustomerById } = useAxios();
  const [customerData, setCustomerData] = useState(null);
  const [customerDatas, setCustomerDatas] = useState(null);
  const { id } = useParams();
  const {
    data: customers,
    isLoading,

  } = 
  useQuery(
    {
      queryKey: ['customers',id],
      queryFn: async () => await getTicketById(id),
    }
  );

  const {
    data: customer,
  } = 
  useQuery(
    {
      queryKey: ['customer',id],
      queryFn: async () => await getCustomerById(id),
    }
  );
  useEffect(() => {
    if (customer) {
      setCustomerDatas(customer);
      console.log(customer);
    }
  }, [customer]);
  useEffect(() => {
    if (customers) {
      setCustomerData(customers);
      console.log(customers);
    }
  }, [customers]);

  function handleOnClick(id){
    console.log('Co Nhan nha');
    setEditOpenModal(true);
    setId(id);
  }
  return (
    <>
    {!isLoading ? (
      <Box>
        <Paper elevation={3} style={{ padding: '20px' }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12}>
              <Typography variant="h5" gutterBottom>
                Ticket Detail
                <IconButton color="primary" aria-label="edit ticket">
                  <EditIcon />
                </IconButton>
              </Typography>
              <Divider style={{ marginBottom: '10px' }} />
            </Grid>
            <Grid item xs={12}>
              <Typography variant="subtitle1">Tag</Typography>
              <Typography>#Tag #Tag #Tag</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">ID:</Typography>
              <Typography>{customers.id}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Title:</Typography>
              <Typography>Tiêu đề</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Description:</Typography>
              <Typography>{customers.note}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Ngày Tạo:</Typography>
              <Typography>{customers.createdBy}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Status:</Typography>
              <Typography>{customers.ticketStatusId}</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Priority:</Typography>
              <Typography>
                <PriorityHighIcon style={{ color: 'red' }} />
                {customers.levelId}
              </Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Assignee:</Typography>
              <Typography>Huỳnh Chí Cường</Typography>
            </Grid>
            <Grid item xs={6}>
              <Typography variant="subtitle1">Type</Typography>
              <Typography>{customers.ticketTypeId}</Typography>
            </Grid>
          </Grid>
        </Paper>
        <Box mb='10px'>
          <Paper elevation={3} style={{ padding: '20px' }}>
            <Grid container spacing={2} alignItems="center">
              <Grid item xs={12}>
                <Typography variant="h5" gutterBottom>
                  Khách Hàng
                  <IconButton color="primary" aria-label="edit ticket">
                    <EditIcon />
                  </IconButton>
                </Typography>
                <Divider style={{ marginBottom: '10px' }} />
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">ID:</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">TcustomerLevelId</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">sourceId</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">branchId</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">phoneNumber</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">address</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">dayOfBirth</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">name</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">gender</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="subtitle1">status</Typography>
                <Typography>Ticket Detail</Typography>
              </Grid>
            </Grid>
          </Paper>
        </Box>
      </Box>
    ) : null}
    </>
    

  );
}
