import React from 'react'
import './sidebar.css'
//Thêm hình
import logo from '../../assets/tele.jpg'
//Thêm Icon
import { IoMdSpeedometer  } from 'react-icons/io'
import { GrSchedules  } from 'react-icons/gr'
import { CiHeadphones } from 'react-icons/ci'
import { MdHistoryToggleOff  } from 'react-icons/md'
import { BiTask  } from 'react-icons/bi'
import PhoneMissedIcon from '@mui/icons-material/PhoneMissed';

import { useNavigate, useLocation } from 'react-router-dom';

const SideBar = () => {
    const navigate = useNavigate();

    const location = useLocation();
    return (
        <div className='sideBar grid'>

            <div className="logoDiv flex">
                <img src={logo} alt='Image Name' />
            </div>

            <div className="menuDiv">

                <ul className='menuLists grid'>
                <li className={'listItem ' + 
                        (location.pathname === '/staff' ? 'active' : '')
                    }
                    onClick={() => navigate('/staff')}>
                        <a  className='menuLink flex' > 
                            <CiHeadphones  className='icon' />
                            <span className="smallText">
                                Khách Hàng
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' + 
                        (location.pathname === '/staff/callhistory' ? 'active' : '')
                    }
                    onClick={() => navigate('/staff/callhistory')}>
                        <a className='menuLink flex' >
                            <MdHistoryToggleOff className='icon' />
                            <span className="smallText">
                                Lịch Sử Cuộc Gọi
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' + 
                        (location.pathname === '/staff/ticket' ? 'active' : '')
                    }
                    onClick={() => navigate('/staff/ticket')}>
                        <a  className='menuLink flex' >
                            <BiTask  className='icon' />
                            <span className="smallText">
                                Ticket
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' + 
                        (location.pathname === '/staff/schudule' ? 'active' : '')
                    }
                    onClick={() => navigate('/staff/schudule')}>
                        <a className='menuLink flex' >
                            <GrSchedules  className='icon' />
                            <span className="smallText">
                                Đặt Lịch
                            </span>
                        </a>
                    </li>

                    <li className={'listItem ' + 
                        (location.pathname === '/staff/misscall' ? 'active' : '')
                    }
                    onClick={() => navigate('/staff/misscall')}>
                        <a className='menuLink flex' >
                            <PhoneMissedIcon  className='icon' />
                            <span className="smallText">
                                Kiểm Tra Gọi Nhỡ
                            </span>
                        </a>
                    </li>
                </ul>
            </div>

            <div className="sideBarCard">
                <IoMdSpeedometer className='icon' />
                <div className="cardContent">
                    <div className="circle1"></div>
                    <div className="circle1"></div>
                    <h3>Trung tâm trợ giúp</h3>
                    <p>Chúc bạn có ngày làm việc vui vẻ.</p>
                    <button className='btn'>Xin Chào</button>
                </div>
            </div>
        </div>
    )
}

export default SideBar