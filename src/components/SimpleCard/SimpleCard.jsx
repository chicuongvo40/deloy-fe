import { Card, Box, styled } from "@mui/material";
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
// STYLED COMPONENTS
const CardRoot = styled(Card)({
  height: "100%",
  padding: "20px 24px",
  backgroundColor: 'hsl(0, 0%, 100%)'
});

const CardTitle = styled("div")(({ subtitle }) => ({
  fontSize: "1rem",
  fontWeight: "500",
  textTransform: "capitalize",
  marginBottom: !subtitle && "16px"
}));
//hsl(0, 0%, 100%)
export default function SimpleCard({ children, title, subtitle,setOpenModal }) {
  return (
    <CardRoot elevation={6}>
      <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <CardTitle subtitle={subtitle} sx={{
        fontWeight: 'bold',
      }}>{title}</CardTitle>
          </Grid>
          <Grid item>
            <Button onClick={() => setOpenModal(true)} variant="contained" color="inherit" sx={{
        fontWeight: 'bold',
      }}>
             Thêm {title}
            </Button>
          </Grid>
        </Grid>
      {subtitle && <Box mb={2}>{subtitle}</Box>}
      {children}
    </CardRoot>
  );
}
