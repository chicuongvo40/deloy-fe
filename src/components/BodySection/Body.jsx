import React from 'react'
import './body.css'

import { BiSearchAlt } from 'react-icons/bi'
import { TbMessageCircle } from 'react-icons/tb'
import { MdOutlineNotificationsNone } from 'react-icons/md'
import { BsArrowRightShort } from 'react-icons/bs'

import img from '../../assets/profile.jpeg'
import img2 from '../../assets/lg.png'
import video from '../../assets/fish.mp4'

const Body = () => {
  return (
    <div className='topSection1'>
      <div className="headerSection flex">
        <div className="title">
        <h1>Chào mừng đến với Amazing Tech</h1>
          <p>Hello Cuong, Chào mừng trở lại!!!</p>
        </div>

        <div className="searchBar flex">
          <input type='text' placeholder='Search' />
          <BiSearchAlt className='icon' />
        </div>

        <div className="adminDiv flex">
          <TbMessageCircle className='icon' />
          <MdOutlineNotificationsNone className='icon' />
          <div className='adminImage'>
            <img src={img} alt='Admin Image' />
          </div>
        </div>
      </div>

      <div className='cardSection flex'>
        
        <div className="rightCard flex">
          <div className="videoDiv">
            <video src={video} autoPlay loop muted></video>
          </div>
        </div>

        <div className="leftCard flex">
          <div className="main flex">

            <div className="imgDiv">
              <img src={img2} alt='Image Name' />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Body