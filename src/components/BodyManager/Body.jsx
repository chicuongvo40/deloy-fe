import React from 'react'
import './body.css'

import { BiSearchAlt } from 'react-icons/bi'
import { TbMessageCircle } from 'react-icons/tb'
import { MdOutlineNotificationsNone } from 'react-icons/md'
import { BsArrowRightShort } from 'react-icons/bs'
import img from '../../assets/profile.jpeg'
import img2 from '../../assets/lg.png'
import video from '../../assets/fish.mp4'

const BodyManager = () => {
  return (
    <div className='topSection1'>
      <div className="headerSection flex">
        <div className="title">
          <h1>Welcom to Amazing Tech</h1>
          <p>Hello Cuong, Welcome Back!!!</p>
        </div>

        <div className="searchBar flex">
          <input type='text' placeholder='Search' />
          <BiSearchAlt className='icon' />
        </div>

        <div className="adminDiv flex">
          <TbMessageCircle className='icon' />
          <MdOutlineNotificationsNone className='icon' />
          <div className='adminImage'>
            <img src={img} alt='Admin Image' />
          </div>
        </div>
      </div>

      <div className='cardSection flex'>
        
        <div className="rightCard flex">
          <div className="videoDiv">
            <video src={video} autoPlay loop muted></video>
          </div>
        </div>

        <div className="leftCard flex">
          <div className="main flex">

            <div className="textDiv">
              <h1>My Stat</h1>

              {/* <div className="flex">
                <span>
                  Today <br /> <small>4 Orders</small>
                </span>
                <span>
                  This Month <br /> <small>127 Orders</small>
                </span>
              </div>

              <span className='flex link'>
                Go to my order <BsArrowRightShort className='icon' />
              </span> */}
            </div>
            <div className="imgDiv">
              <img src={img2} alt='Image Name' />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default BodyManager