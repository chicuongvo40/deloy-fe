// modules
import { createContext, useContext, useMemo } from 'react';
import Axios from 'axios';

// Axios context
const AxiosContext = createContext();

export default function AxiosProvider({ children }) {
  const axios = useMemo(() => {
    const axios = Axios.create({
      baseURL: 'https://cuong.twentytwo.asia',
      headers: {

        'Content-Type': 'application/json-patch+json',
      },
    });

    axios.interceptors.request.use((config) => {
      const token = localStorage.getItem('token')
        ? JSON.parse(localStorage.getItem('token'))
        : '';
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }

      return config;
    });
    axios.interceptors.response.use(
      (response) => response,
      async (error) => {
        console.log(error.response.status + 'AAAAAAAAAA');
        //! Nếu token hết hạn tự động logout
        if (error.response.status === 401) {
          localStorage.setItem('user', null);
          localStorage.setItem('token', null);
        }
        return Promise.reject(error);
      },
    );
    return axios;
  }, []);

  //* Đăng nhập
  const loginUser = (data) =>
    axios.post('/login', data)
      .then((res) => res.data)
      .catch((error) => {
        if (error.response) {
          console.error('Server responded with an error:', error.response.data);
        } else if (error.request) {
          console.error('No response received from server:', error.request);
        } else {
          console.error('Error setting up the request:', error.message);
        }
        // Trả về một promise bị reject với lỗi gốc để cho phép các thành phần khác xử lý
        return Promise.reject(error);
      });

  //GetCustomer
  const getCustomer = () =>
    axios
      .get(`/api/customer?page=1&size=100`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
  //GetCustomerById
  const getCustomerById = (id) =>
    axios
      .get(`/api/customer/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //GetContract
  const getContract = () =>
    axios
      .get(`/api/contract/1`)
      .then((res) => res.data)
      .catch((err) => err.response.data);

  //GetTicket
  const getTicket = () =>
    axios
      .get(`api/ticket?page=1&size=100`)
      .then((res) =>
        res.data.results)
      .catch((err) => err.response.data);
      //GetTicketName
  const getTicketName = () =>
  axios
    .get(`api/ticketname?page=1&size=100`)
    .then((res) =>
      res.data.results)
    .catch((err) => err.response.data);
  //GetCallHistory
  const getCallHistory = () =>
    axios
      .get(`api/callHistory?page=1&size=100`)
      .then((res) =>
        res.data.results)
      .catch((err) => err.response.data);
  //Create Customer
  const postCustomer = ({ data }) =>
    axios
      .post(`/customer`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //Create Ticket
  const postTicket = ({ data }) =>
    axios
      .post(`/ticket`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //Get Brand
  const getBrand = () =>
    axios
      .get(`/api/branch?page=1&size=100`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
  //Post Brand
  const postBrand = ({ data }) =>
    axios
      .post(`/branch`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //GetBranchId  
  const getBranchById = (id) =>
    axios
      .get(`/api/branch/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //UpdateBranch    
  const updateBranch = ({ data }) =>
    axios
      .put(`/api/branch`, data)
      .then((res) => res.status)
      .catch((err) => err.response.data);
  //GetLevel
  const getLevel = () =>
    axios
      .get(`/api/level?page=1&size=10`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
  //PostLevel
  const postLevel = ({ data }) =>
    axios
      .post(`/level`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //GetLevelId  
  const getLevelById = (id) =>
    axios
      .get(`/api/level/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //UpdateLevel    
  const updateLevel = ({ data }) =>
    axios
      .put(`/api/level`, data)
      .then((res) => res.status)
      .catch((err) => err.response.data);
  //GetTicketType  
  const getTicketType = (id) =>
    axios
      .get(`/api/tickettype?page=1&size=10`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
  //GetTicketTypeById  
  const getTicketTypeById = (id) =>
    axios
      .get(`/api/tickettype/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //UpdateTicketType    
  const updateTicketType = ({ data }) =>
    axios
      .put(`/api/tickettype`, data)
      .then((res) => res.status)
      .catch((err) => err.response.data);
  //PostLevel
  const postTicketType = ({ data }) =>
    axios
      .post(`/tickettype`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //GetSource
  const getSource = (id) =>
    axios
      .get(`/api/source?page=1&size=100`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
  //UpdateSource
  const updateSource = ({ data }) =>
    axios
      .put(`/api/source`, data)
      .then((res) => res.status)
      .catch((err) => err.response.data);

  //PostSource
  const postSource = ({ data }) =>
    axios
      .post(`/source`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //* Xóa brand theo id
  const deleteBranchById = (id) =>
    axios
      .delete(`/api/branch/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);

  //* Xóa brand theo id
  const deleteLevelById = (id) =>
    axios
      .delete(`/api/level/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);

  //* Xóa brand theo id
  const deleteSourceById = (id) =>
    axios
      .delete(`/api/source/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);

  //* Xóa brand theo id
  const deleteTicketTypeById = (id) =>
    axios
      .delete(`/api/tickettype/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
  //Update Customer
  const updateCustomer = ({ data }) =>
    axios
      .put(`/api/customer`, data)
      .then((res) => res.status)
      .catch((err) => err.response.data);
  //getschedule
  const getschedule = (id) =>
  axios
    .get(`/api/schedule?page=1&size=100`)
    .then((res) => res.data.results)
    .catch((err) => err.response.data);
    //Postschedule
    const postschedule = ({ data }) =>
    axios
      .post(`/schedule`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
     //getstaff
  const getStaff = (id) =>
  axios
    .get(`/api/User?page=1&size=100`)
    .then((res) => res.data.results)
    .catch((err) => err.response.data);
    //GetReportCall
    const getReportCall = ({ startDate, endDate }) =>
    axios
      .get(`/api/report/callhistory?DateStart=${startDate}&DateEnd=${endDate}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
    //GetReport
    const getReportTicket = ({ startDate, endDate }) =>
    axios
      .get(`/api/report/ticket?DateStart=${startDate}&DateEnd=${endDate}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
    //UpdateTicket    
  const updateTicket = ({ data }) =>
  axios
    .put(`/api/ticket`, data)
    .then((res) => res.status)
    .catch((err) => err.response.data);
    //GetTicketById  
    const getTicketById = (id) =>
    axios
      .get(`/api/ticket/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
    //GetUser
    const getUserById = (id) =>
    axios
      .get(`/api/user/${id}`)
      .then((res) => res.data)
      .catch((err) => err.response.data);
    //getCustomerLevel
    const getCustomerLevel = () =>
    axios
      .get(`/api/customerlevel?page=1&size=10`)
      .then((res) => res.data.results)
      .catch((err) => err.response.data);
      //Chua lam
      const getContractById = (id) =>
      axios
        .get(`api/contract/customerId?customerId=${id}`)
        .then((res) => res.data)
        .catch((err) => err.response.data);
        //postCallHis
        const postCallHistory = ({ data }) =>
        axios
          .post(`/callHistory`, data)
          .then((res) => res.data)
          .catch((err) => err.response.data);
        //GetTicketByCustomerId  
  const getTicketByCustomerId  = (id) =>
  axios
    .get(`/api/ticketname/customerId?customerId=${id}`)
    .then((res) => res.data)
    .catch((err) => err.response.data);
    const getCallHistoryByCustomerId  = (id) =>
  axios
    .get(`/api/callHistory/customerId?customerId=${id}`)
    .then((res) => res.data)
    .catch((err) => err.response.data);

    const getMissCall = ({ data }) =>
    axios
      .post(`/api/cloudfone/get-call-history`, data)
      .then((res) => res.data)
      .catch((err) => err.response.data);
      
      const getTicketByStatus  = (id) =>
      axios
        .get(`https://cuong.twentytwo.asia/api/tickettag/statusId?statusId=${id}`)
        .then((res) => res.data)
        .catch((err) => err.response.data);
      
        const getScheduleByTitle  = (id) =>
      axios
        .get(`/api/schedule/${id}`)
        .then((res) => res.data)
        .catch((err) => err.response.data); 
          const getTicketTag = () =>
          axios
            .get(`/api/tickettag?page=1&size=100`)
            .then((res) => res.data.results)
            .catch((err) => err.response.data);

            const postCustomerLevel = ({ data }) =>
            axios
              .post(`/customerlevel`, data)
              .then((res) => res.data)
              .catch((err) => err.response.data);

                //UpdateBranch    
  const updateCustomerLevel = ({ data }) =>
  axios
    .put(`/api/customerlevel`, data)
    .then((res) => res.status)
    .catch((err) => err.response.data);
  return (
    <AxiosContext.Provider
      value={{
        loginUser,
        getCustomer,
        getContract,
        getTicket,
        getCallHistory,
        getCustomerById,
        postCustomer,
        postTicket,
        getBrand,
        postBrand,
        getBranchById,
        updateBranch,
        getLevel,
        postLevel,
        getLevelById,
        updateLevel,
        getTicketType,
        getTicketTypeById,
        updateTicketType,
        postTicketType,
        getSource,
        postSource,
        updateSource,
        deleteBranchById,
        deleteLevelById,
        deleteSourceById,
        deleteTicketTypeById,
        updateCustomer,
        getschedule,
        postschedule,
        getStaff,
        getReportCall,
        getReportTicket,
        getTicketName,
        updateTicket,
        getTicketById,
        getUserById,
        getCustomerLevel,
        getContractById,
        postCallHistory,
        getTicketByCustomerId,
        getCallHistoryByCustomerId,
        getMissCall,
        getTicketByStatus,
        getScheduleByTitle,
        getTicketTag,
        postCustomerLevel,
        updateCustomerLevel
      }}
    >
      {children}
    </AxiosContext.Provider>
  );
}

export function useAxios() {
  return useContext(AxiosContext);
}
