//modules
import { createContext, useEffect, useState, useContext } from 'react';
import { useMutation } from '@tanstack/react-query';
import { jwtDecode } from 'jwt-decode';
import { Navigate } from 'react-router-dom';
import { useAxios } from './AxiosContex';
import { useLocalStorage } from '../hooks/useStorage';
const Context = createContext();
export function useAuth() {
  return useContext(Context);
}

export function AuthProvider({ children }) {
  //* Đăng nhập link
  const { loginUser } = useAxios();
  //* Localstorage
  const [user, setUser, removeUser] = useLocalStorage('user', null);
  const [token, setToken, removeToken] = useLocalStorage('token', null);
  //* Message trả về khi đăng nhập
  const [message, setMessage] = useState(null);
  // Login
  useEffect(() => {
    if (!token) return;
    const jwtUser = jwtDecode(token);
    setUser(jwtUser);
  }, [token]);

  //* Login mutation
  const login = useMutation({
    mutationFn: (data) => {
      return loginUser(data)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          console.log('login fail nhe');
          <Navigate
            to={`/staff`}
            replace
          />
          setMessage(err.response.data.message);
        });
    },
    onSuccess(data) {
      setToken(data?.jwt);
      if (user) {
        return (
          <Navigate
            to={`/staff`}
            replace
          />
        );
      }
    },
  });

  return (
    <Context.Provider
      value={{
        removeUser,
        removeToken,
        token,
        login,
        user,
        message,
      }}
    >
      {children}
    </Context.Provider>
  );
}
