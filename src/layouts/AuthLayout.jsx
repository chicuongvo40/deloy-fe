import { Navigate, Outlet } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

export function AuthLayout() {
  const { user } = useAuth();
  if (user) {
    return <Navigate to={`/staff`} />;
  }
  return <Outlet />;
}