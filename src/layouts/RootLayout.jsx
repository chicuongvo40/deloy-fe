import React from "react";
import { Navigate,Outlet } from "react-router-dom";
import './RootLayout.css';
import { useSip } from '../context/SipContext';
import Body from "../components/BodySection/Body";
import SideBar from '../components/SideBarSection/SideBar'
import { useAuth } from '../context/AuthContext';
import {  useState,useEffect } from 'react';
import { FormModal } from '../components/CallingModal';
export function RootLayout() {
  const { user} = useAuth();
  const [openFormCall, setOpenFormCall] = useState(false);
  const { deviceclv } = useSip();
  
  function setStatus(){
    setOpenFormCall(true);

  }

  function setOffStatus(){
    setOpenFormCall(false);

  }
 
  if (deviceclv?.current) {
    deviceclv.current.on('invite', (data) => {
      console.log('Co nguoi goi toi ne' + Date.now());
      console.log(data);
      setStatus();
    });
  }
  function handleOnClick(){
    console.log("Da nhan nghe duoc dth");
    setOffStatus();
    deviceclv.current.accept();
  }
  // if (user === null) return <Navigate to='/login' />;
  console.log(user);
  return (
    <>
    {openFormCall ? (
      <FormModal
      handleOnClick={handleOnClick}
      setOffStatus={setOffStatus}
    />) : null
    }
    <div className="container2">
      <SideBar />
      <div className="mainContent2">
        <Body />
        <div className="bottom2 flex">
          <Outlet />
        </div>
        </div>
    </div>
    </>
  );
}
